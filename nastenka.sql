-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost
-- Vytvořeno: Stř 10. čen 2020, 08:04
-- Verze serveru: 10.3.22-MariaDB-log
-- Verze PHP: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `tuma02`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `Employee`
--

CREATE TABLE `Employee` (
  `userId` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_czech_ci NOT NULL,
  `email` varchar(40) COLLATE utf8mb4_czech_ci NOT NULL,
  `phone` int(12) NOT NULL,
  `idVed` int(11) DEFAULT NULL,
  `pass` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `Employee`
--

INSERT INTO `Employee` (`userId`, `name`, `email`, `phone`, `idVed`, `pass`) VALUES
(1, 'Admin', 'dok@mail.ua', 31654879, 1, '$2y$10$pUbFpZaGtrkpzQ6VZYNBtOBi64xAnt7kTikr./HENBGmFUjrX9k12'),
(8, 'Alex Makalister', 'alex@mail.cz', 102346582, 1, '$2y$10$fjeU.CfN7nVUjS7szKyLOOiQSutrxAowdWaZYbfONaTQIDQu4i6Oq'),
(9, 'Sava Shustry', 'sava@mail.ua', 120103110, 8, '$2y$10$EPvi5Cpl8dTmYlnK4eXpj.EQLLj.yOJnafZr1OGW8vAJYhNfrDDYe');

-- --------------------------------------------------------

--
-- Struktura tabulky `Goods`
--

CREATE TABLE `Goods` (
  `productId` int(11) NOT NULL,
  `producerId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `title` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `Goods`
--

INSERT INTO `Goods` (`productId`, `producerId`, `quantity`, `title`) VALUES
(1, 3, 2, 'Notebook MP10'),
(2, 7, 2, 'Computer 12.0.3'),
(3, 7, 1, 'Elektrokytara'),
(4, 8, 2, 'Headphones 15Lite'),
(5, 6, 0, 'Macbook Pro'),
(6, 9, 1, 'Mobil P40'),
(7, 4, 11, 'Mouse 13Pro'),
(8, 5, 1, 'Redmi 12'),
(9, 2, 5, 'Calculator'),
(10, 10, 0, 'Bag 120'),
(11, 7, 3, 'Mediator'),
(12, 9, 0, 'Headphones 15Lite'),
(13, 4, 1, 'Whiteboard'),
(14, 5, 1, 'Flipchart'),
(15, 8, 2, 'Speakers 35'),
(16, 2, 9, 'Writing Set'),
(17, 10, 2, 'Java Book'),
(18, 10, 2, 'PHP book'),
(19, 6, 6, 'Info systems book'),
(20, 7, 2, 'Drums'),
(21, 7, 6, 'Microphone 14');

-- --------------------------------------------------------

--
-- Struktura tabulky `Loans`
--

CREATE TABLE `Loans` (
  `loanId` int(5) NOT NULL,
  `userId` int(5) NOT NULL,
  `productId` int(5) NOT NULL,
  `borrowDate` date NOT NULL,
  `returned` tinyint(1) NOT NULL,
  `renewDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `Loans`
--

INSERT INTO `Loans` (`loanId`, `userId`, `productId`, `borrowDate`, `returned`, `renewDate`) VALUES
(29, 1, 1, '2020-06-09', 0, '2020-06-29'),
(30, 1, 2, '2020-06-09', 0, '2020-06-29'),
(31, 1, 3, '2020-06-09', 1, '2020-06-29'),
(32, 1, 7, '2020-06-09', 0, '2020-06-29'),
(33, 1, 10, '2020-06-09', 0, '2020-06-29'),
(34, 1, 1, '2020-06-09', 1, '2020-06-29'),
(35, 9, 1, '2020-06-10', 0, '2020-06-30'),
(36, 9, 7, '2020-06-10', 0, '2020-06-30'),
(37, 9, 10, '2020-06-10', 0, '2020-06-30'),
(38, 9, 12, '2020-06-10', 0, '2020-06-30');

-- --------------------------------------------------------

--
-- Struktura tabulky `Producer`
--

CREATE TABLE `Producer` (
  `producerId` int(11) NOT NULL,
  `title` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `Producer`
--

INSERT INTO `Producer` (`producerId`, `title`) VALUES
(1, 'HP'),
(2, 'Citizen'),
(3, 'Acer'),
(4, 'Razor'),
(5, 'Xiomi'),
(6, 'Apple'),
(7, 'ABX'),
(8, 'Panasonic'),
(9, 'Black View'),
(10, 'Valve');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `Employee`
--
ALTER TABLE `Employee`
  ADD PRIMARY KEY (`userId`);

--
-- Klíče pro tabulku `Goods`
--
ALTER TABLE `Goods`
  ADD PRIMARY KEY (`productId`),
  ADD KEY `producerId` (`producerId`);

--
-- Klíče pro tabulku `Loans`
--
ALTER TABLE `Loans`
  ADD PRIMARY KEY (`loanId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `productId` (`productId`);

--
-- Klíče pro tabulku `Producer`
--
ALTER TABLE `Producer`
  ADD PRIMARY KEY (`producerId`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `Employee`
--
ALTER TABLE `Employee`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pro tabulku `Goods`
--
ALTER TABLE `Goods`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pro tabulku `Loans`
--
ALTER TABLE `Loans`
  MODIFY `loanId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pro tabulku `Producer`
--
ALTER TABLE `Producer`
  MODIFY `producerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `Goods`
--
ALTER TABLE `Goods`
  ADD CONSTRAINT `Goods_ibfk_1` FOREIGN KEY (`producerId`) REFERENCES `Producer` (`producerId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `Loans`
--
ALTER TABLE `Loans`
  ADD CONSTRAINT `Loans_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Employee` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Loans_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `Goods` (`productId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
