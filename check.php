<?php
/*Soubor kontroluje udaje zapsane ve formě registrace(first)
a když jsou údaje spravné, udělavá insert*/
$email = filter_var(trim($_POST['email']), FILTER_SANITIZE_STRING);
$pass = filter_var(trim($_POST['pass']), FILTER_SANITIZE_STRING);
$pass2 = filter_var(trim($_POST['pass2']), FILTER_SANITIZE_STRING);
$name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
$vedouci = filter_var(trim($_POST['vedouci']), FILTER_SANITIZE_STRING);
$phone = filter_var(trim($_POST['phone']), FILTER_SANITIZE_STRING);

setcookie('r_email', $email, time() - 3600, "/");
setcookie('r_pass', $pass, time() - 3600, "/");
setcookie('r_name', $name, time() - 3600, "/");
setcookie('r_vedouci', $vedouci, time() - 3600, "/");
setcookie('r_phone', $phone, time() - 3600, "/");

setcookie('r_email', $email, time() + 3600, "/");
setcookie('r_pass', "", time() + 3600, "/");
setcookie('r_name', $name, time() + 3600, "/");
setcookie('r_vedouci', $vedouci, time() + 3600, "/");
setcookie('r_phone', $phone, time() + 3600, "/");

setcookie('error_email1', "Email byl zapsan nekorektně", time() - 3600, "/");
setcookie('error_name1', "Jmeno bylo zapsano nekorektně", time() - 3600, "/");
setcookie('error_email2', "Nekorektní delka emailu", time() - 3600, "/");
setcookie('error_name2', "Nekorektní delka jmena", time() - 3600, "/");
setcookie('error_pass', "Nekorektní delka hesla(od 2 do 10 symbolů)", time() - 3600, "/");
setcookie('error_phone', "Telefonní čislo je zapsano nekorektně", time() - 3600, "/");
setcookie('error_vedouci', "ID vedoucího je zapsano nekorektně", time() - 3600, "/");
setcookie('error_email3', "Takový email už je registrovan", time() - 3600, "/");
setcookie('r_passes', "Uvedená hesla se ne rovnají", time() - 3600, "/");
$errors = 0;
require "connect.php";
$sql = "SELECT * FROM Employee WHERE email = '$email'";
$result = $mysql->query($sql);
$user = $result->fetch_assoc();
if($user != null){
    setcookie('error_email3', "Takový email už je registrovan", time() + 3600, "/");
    $errors++;
}
if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
    setcookie('error_email1', "Email byl zapsan nekorektně", time() + 3600, "/");
    $errors++;
}
if(!preg_match("/^[a-zA-Z ]*$/",$name)){
    setcookie('error_name1', "Jmeno bylo zapsano nekorektně", time() + 3600, "/");
    $errors++;
}
if(mb_strlen($email) > 40 || mb_strlen($email) < 3){
    setcookie('error_email2', "Nekorektní delka emailu", time() + 3600, "/");
    $errors++;
}
if(mb_strlen($name) > 30 || mb_strlen($name) < 2){
    setcookie('error_name2', "Nekorektní delka jmena", time() + 3600, "/");
    $errors++;
}
if(mb_strlen($pass) > 10 || mb_strlen($pass) < 3){
    setcookie('error_pass', "Nekorektní delka hesla(od 2 do 10 symbolů)", time() + 3600, "/");
    $errors++;
}
if($pass != $pass2){
    setcookie('r_passes', "Uvedená hesla se ne rovnají", time() + 3600, "/");
    $errors++;
}

if((mb_strlen($phone) !=  12 && mb_strlen($phone) != 9) || !is_numeric($phone)){
    setcookie('error_phone', "Telefonní čislo je zapsano nekorektně)", time() + 3600, "/");
    $errors++;
}
if((!is_numeric($vedouci) && mb_strlen($vedouci) > 0) || $vedouci<0){
    setcookie('error_vedouci', "ID vedoucího je zapsano nekorektně)", time() + 3600, "/");
    $errors++;
}
if($vedouci == null){
    $vedouci = 0;
}

if($errors > 0){
    header('Location: index.php');
    exit();
}
$emailPass = $pass;
//Hashing function for password
$pass = password_hash($pass, PASSWORD_DEFAULT);

require "connect.php";
$sql = "INSERT INTO Employee (name, email, phone, idVed, pass) VALUES ('$name','$email', '$phone', '$vedouci', '$pass')";
if($mysql->query($sql) === TRUE){
    echo "Record created";
}
$mysql->close();
/*odesila se mail uživateli s udajé o registraci*/
$message = "Dobrý den.".'<br>'."Jsme rádi, že pžipojily jste naš tým".'<br>'."Vaš login: ".$email.'<br>'."Vaše heslo: ".$emailPass;
$to = $email;
$from = "admin@mail.ua";
$subject = "Registrace. Provider";
$subject = "=?utf-8?B?".base64_encode($subject)."?=";
$headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/plain; charset= utf-f\r\n";

if(mail($to, $subject, $message, $headers) === TRUE){
    echo '<br>'."Otpravleno";
}

setcookie('registered', "ano", time() + 3600, "/");
header('Location: user.php');
?>
