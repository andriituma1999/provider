<?php
/*Stranka, která jde po přihlašení, nebo registrace. Tady uivatelu bude nabidnut interface
v zavislosti na tom je-li on admin*/
/**/
?>
<?php
require "userHead.php";
?>
<div class="container mt-4">

    <?php
    /*přiřazení offsetu. Offset slouži pro strankovaní*/
    if (isset($_GET['offset'])) {
        $offset = (int)$_GET['offset'];
    } else {
        $offset = 1;
    }
    /*konec přiřazení offsetu*/
    require "connect.php";
    $sql = "SELECT * FROM Goods WHERE quantity > 0";
    $result = $mysql->query($sql);
    $user = $result->fetch_all(MYSQLI_ASSOC);
    $count = count($user);
    /*dale jde zapsaní tabulky, přiřazení sloupcu a řadku*/
    if(count($user) > 0) {?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Nazev</th>
                <th>Producer</th>
                <th>Počet</th>
                <th>Pujčit</th>
            </tr>

            <?

            for($a = ($offset - 1) * 10; $a < $offset * 10; $a++){
                if($a > $count - 1){
                    continue;}
                $id = $user[$a]['producerId'];
                $sql = "SELECT * FROM Producer WHERE producerId = '$id'";
                $result = $mysql->query($sql);
                $use = $result->fetch_assoc();
                $name = $use['title'];
                ?>

                <tr>
                    <td>
                        <? echo $user[$a]['title']; ?>
                    </td>
                    <td>
                        <? echo $name; ?>
                    </td>
                    <td>
                        <? echo $user[$a]['quantity']; ?>
                    </td>
                    <td>
                        <a class="btn btn-primary" href="borrow.php?borrown=<?=$a+1?>" role="button">+</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <div>
            <ul class=" pagination justify-content-center">
                <?php
                //vypsaní tlačitek strankování
                for($i=1; $i<=ceil($count/10); $i++){
                    echo '<li class="page-item"><a class="text-info page-link" href="user.php?offset='.$i.'">'.$i.'</a></li>';
                }
                ?>
            </ul>
        </div>
        <br />
        <?php
    }
    $mysql->close();
    require "footer.php";
    ?>
