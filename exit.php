<?php
//Soubor zpracovava funkce Odhlasit se
setcookie('error_email1', "Email byl zapsan nekorektně", time() - 3600, "/");
setcookie('error_name1', "Jmeno bylo zapsano nekorektně", time() - 3600, "/");
setcookie('error_email2', "Nekorektní delka emailu", time() - 3600, "/");
setcookie('error_name2', "Nekorektní delka jmena", time() - 3600, "/");
setcookie('error_pass', "Nekorektní delka hesla(od 2 do 10 symbolů)", time() - 3600, "/");
setcookie('error_phone', "Telefonní čislo je zapsano nekorektně", time() - 3600, "/");
setcookie('error_vedouci', "ID vedoucího je zapsano nekorektně", time() - 3600, "/");
setcookie('error_email3', "Takový email už je registrovan", time() - 3600, "/");

//deaktivace všech cookie, pouitych při registrace nebo autorizace
setcookie('r_email', $email, time() - 3600, "/");
setcookie('r_pass', $pass, time() - 3600, "/");
setcookie('r_name', $name, time() - 3600, "/");
setcookie('r_vedouci', $vedouci, time() - 3600, "/");
setcookie('r_phone', $phone, time() - 3600, "/");

setcookie('a_email', $email, time() - 3600, "/");
setcookie('a_pass', $pass, time() - 3600, "/");
setcookie('error_aut', "Takového uživatele v systemu není", time() - 3600, "/");

setcookie('user', $user['name'], time() - 3600, "/");
setcookie('registered', "ne", time() - 3600, "/");
setcookie('authorized', "ne", time() - 3600, "/");
header('Location: index.php');
?>