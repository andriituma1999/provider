<?php
require "userHead.php";
/*Stranka slouži pro vyvedení formy pro změnu zboži*/
?>
<div class="container mt-4>
    <div class="row">
        <div class="col">
            <h1>Forma změny zboži</h1>
            <form action="checkChange.php" method="post">
                <?php
                $changing = 0;
                if (isset($_GET['changing'])) {
                    $changing = (int)$_GET['changing'];
                } else {
                    echo "nichera ne vyšlo";
                }
                require "connect.php";
                //přiřazení jako cookie čislo zboži, ktere bude změněno
                setcookie('changingGood', $changing, time() + 3600, "/");
                $sql = "SELECT * FROM Goods WHERE productId = '$changing'";
                $result = $mysql->query($sql);
                $user = $result->fetch_assoc();
                $title = $user['title'];
                $producerId = $user['producerId'];
                $quantity = $user['quantity'];
                $sql = "SELECT * FROM Producer WHERE producerId = '$producerId'";
                $result = $mysql->query($sql);
                $user = $result->fetch_assoc();
                $brand = $user['title'];
                     ?>
                <input type="text" class="form-control" name="title"
                       value="<?= $title?>"
                       id="email" placeholder="Uveďte nazev zboži"
                >
                <?php


                if(isset($_COOKIE['error_title']) || isset($_COOKIE['error_title2'])) {
                    $x = 0;
                    if (isset($_COOKIE['error_title'])) {
                        $x++;
                        echo  $_COOKIE['error_title'] ;
                    }
                    if (isset($_COOKIE['error_title2']) && $x == 0) {
                        echo  $_COOKIE['error_title2'] ;
                    }
                    $x = 0;
                }else{
                    echo '<br>';
                }
                ?>
                <input type="text" class="form-control" name="brand"
                       value="<?= $brand?>"
                       id="name" placeholder="Uveďte výrobce zboži">
                <?php


                if(isset($_COOKIE['error_brand']) || isset($_COOKIE['error_brand2']) || isset($_COOKIE['brand3'])) {
                    $x = 0;
                    if (isset($_COOKIE['error_brand'])) {
                        $x++;
                        echo  $_COOKIE['error_brand'] ;
                    }
                    if (isset($_COOKIE['error_brand2']) && $x == 0) {
                        echo  $_COOKIE['error_brand2'] ;
                        $x++;
                    }
                    if (isset($_COOKIE['brand3']) && $x == 0) {
                        echo  $_COOKIE['brand3'] ;
                    }
                    $x = 0;
                }else{
                    echo '<br>';
                }
                ?>
                <input type="number" class="form-control" name="quantity"
                       value="<?= $quantity?>"
                       id="phone" placeholder="Uveďte počet produktu">
                <?php
                if (isset($_COOKIE['error_quantity'])) {
                    echo $_COOKIE['error_quantity'];
                }
                else{
                    echo '<br>';
                }
                ?>
                <button class="btn btn-success" name = "changeGood" type="submit">Změnit</button>
            </form>
        </div>
    </div>
</div>
<main class="container pt-2">