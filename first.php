<?php
/*Soubor slouží jako prvni stranka, kterou vidí user(Registrace, Autorizace)*/
/**/
?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <title>Provider - semestrální práce z předmětu 4IZ278</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>

<body>
<header class="container bg-info">
    <div class="row">
        <div class="col-7">
            <h1 class="text-white py-1 px-2">Provider</h1>
            <h5 class="text-white py-1 px-2"> Aplikace
                slouží pro vypujčení pracovních věci pracovnikem u společnosti.
                Aby vypujčit pracovní device, musite být přihlašený.
            </h5>

        </div>
    </div>
</header>

    <div class="container mt-4">
        <?php
        /*Při opakovanem stisknutí Registrace, v
        každý řadek formy jako value bude přiřazeno to, co uživatel už uvaděl ped chybou
        Je to realizovano pomocí cookie. System hlašení chyb taky je realizovan pomocí cookie*/
        ?>
        <div class="row">
            <div class="col">
                <h1>Forma registrace</h1>
                <form action="check.php" method="post">
                    <input type="email" class="form-control" name="email"
                           value="<?= isset($_COOKIE["r_email"]) ? $_COOKIE["r_email"] : ""?>"
                           id="email" placeholder="Uveďte Vaše e-mailovou adresu"
                          > <?php


                    if(isset($_COOKIE['error_email1']) || isset($_COOKIE['error_email2']) || isset($_COOKIE['error_email3'])) {
                        $x = 0;
                        if (isset($_COOKIE['error_email1'])) {
                            $x++;
                            echo  $_COOKIE['error_email1'] ;
                        }
                        if (isset($_COOKIE['error_email2']) && $x == 0) {
                            echo  $_COOKIE['error_email2'] ;
                            $x++;
                        }
                        if (isset($_COOKIE['error_email3']) && $x == 0) {
                            echo  $_COOKIE['error_email3'] ;
                        }
                        $x = 0;
                    }else{
                        echo '<br>';
                    }
                    ?>

                    <input type="text" class="form-control" name="name"
                           value="<?= isset($_COOKIE["r_name"]) ? $_COOKIE["r_name"] : ""?>"
                           id="name" placeholder="Uveďte Vaše jmeno a příjmení">

                    <?php
                             if(isset($_COOKIE['error_name1']) || isset($_COOKIE['error_name2'])) {
                                 $x = 0;
                                 if (isset($_COOKIE['error_name1'])) {
                                     $x++;
                                     echo  $_COOKIE['error_name1'] ;
                                 }
                                 if (isset($_COOKIE['error_name2']) && $x == 0) {
                                     echo  $_COOKIE['error_name2'] ;
                                 }
                                 $x = 0;
                             }else{
                                 echo '<br>';
                             }
                    ?>
                    <input type="number" class="form-control" name="phone"
                           value="<?= isset($_COOKIE["r_phone"]) ? $_COOKIE["r_phone"] : ""?>"
                           id="phone" placeholder="Uveďte Vaše čislo telefonu">
                    <?php
                    if(isset($_COOKIE['error_phone'])){
                        echo $_COOKIE['error_phone'];
                    }else{
                     echo '<br>';
                    }
                    ?>
                    <input type="number" class="form-control" name="vedouci"
                           value="<?= isset($_COOKIE["r_vedouci"]) ? $_COOKIE["r_vedouci"] : ""?>"
                           id="vedouci" placeholder="Uveďte id Vašého nadřízeneho">
                    <?php
                    if(isset($_COOKIE['error_vedouci'])){
                        echo $_COOKIE['error_vedouci'];
                    }else{
                        echo '<br>';
                    }
                    ?>
                    <input type="password" class="form-control" name="pass"
                           value="<?= isset($_COOKIE["r_pass"]) ? $_COOKIE["r_pass"] : ""?>"
                           id="pass" placeholder="Uveďte Vaše heslo">
                    <?php
                    if(isset($_COOKIE['error_pass'])){
                        echo $_COOKIE['error_pass'].'<br>';
                    }else{
                        echo '<br>';
                    }

                    ?>
                    <input type="password" class="form-control" name="pass2"
                           value=""
                           id="pass" placeholder="Opakujte heslo">
                    <?php
                    if(isset($_COOKIE['r_passes'])){
                        echo $_COOKIE['r_passes'].'<br>';
                    }else{
                        echo '<br>';
                    }

                    ?>
                    <button class="btn btn-success" name = "register" type="submit">Registrace</button>
                </form>
            </div>
            <div class="col">
                <h1>Forma autorizace</h1>
                <form action="auth.php" method="post">
                    <input type="email" class="form-control" name="email"
                           value="<?= isset($_COOKIE["a_email"]) ? $_COOKIE["a_email"] : ""?>"
                           id="email" placeholder="Uveďte Vaše e-mailovou adresu"><br>
                    <input type="password" class="form-control" name="pass" value=""
                           id="pass" placeholder="Uveďte Vaše heslo">
                    <?php
                    if(isset($_COOKIE['error_aut'])){
                        echo $_COOKIE['error_aut'].'<br>';
                    }else{
                        echo '<br>';
                    }

                    ?>
                    <button class="btn btn-success" type="submit">Přihlasit se</button>
                </form>
            </div>
        </div>

    </div>
<main class="container pt-2">