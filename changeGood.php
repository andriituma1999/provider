<?php
require "userHead.php";
/*Soubor slouži pro vytvaření tabulky se zboží, ktere bude moné změnit*/
?>
<div class="container mt-4">
    <?php
    if (isset($_GET['offset'])) {
        $offset = (int)$_GET['offset'];
    } else {
        $offset = 1;
    }
    require "connect.php";
    $sql = "SELECT * FROM Goods ";
    $result = $mysql->query($sql);
    $user = $result->fetch_all(MYSQLI_ASSOC);
    $count = count($user);
    if(count($user) > 0) { ?>
    <table class="table table-striped table-hover">
        <tr>
            <th>ID</th>
            <th>Nazev</th>
            <th>Producer</th>
            <th>Počet</th>
            <th>Změnit</th>

        </tr>

        <?

        for($a = ($offset - 1) * 10; $a < $offset * 10; $a++){
            if($a > $count - 1){
                continue;}
            $id = $user[$a]['productId'];
            $title = $user[$a]['title'];
            $producerId = $user[$a]['producerId'];
            $quantity = $user[$a]['quantity'];
            $sql = "SELECT * FROM Producer WHERE producerId = '$producerId'";
            $result = $mysql->query($sql);
            $use = $result->fetch_assoc();
            $brand = $use['title'];
            ?>

            <tr>
                <td>
                    <? echo $id; ?>
                </td>
                <td>
                    <? echo $title; ?>
                </td>
                <td>
                    <? echo $brand; ?>
                </td>
                <td>
                    <? echo $quantity; ?>
                </td>
                <td>
                    <a class="btn btn-warning" href="localChangeGood.php?changing=<?=$a+1?>" role="button">+</a>
                </td>
            </tr>
        <?php } ?>
    </table>
        <div>
            <ul class=" pagination justify-content-center">
                <?php
                for($i=1; $i<=ceil($count/10); $i++){
                    echo '<li class="page-item"><a class="text-info page-link" href="changeGood.php?offset='.$i.'">'.$i.'</a></li>';
                }
                ?>
            </ul>
        </div>
        <br />
        <?php
    }
    $mysql->close();
    require "footer.php";
    ?>
