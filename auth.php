<?php
/*Soubor kontroluje udaje zapsane ve formě autorizace(first)
a když jsou údaje spravné, propouští dal*/
$email = filter_var(trim($_POST['email']), FILTER_SANITIZE_STRING);
$pass = filter_var(trim($_POST['pass']), FILTER_SANITIZE_STRING);

setcookie('a_email', $email, time() - 3600, "/");
setcookie('a_pass', $pass, time() - 3600, "/");
setcookie('error_aut', "Takového uživatele v systemu není", time() - 3600, "/");

setcookie('a_email', $email, time() + 3600, "/");
setcookie('a_pass', "", time() - 3600, "/");

require "connect.php";
$sql = "SELECT * FROM Employee WHERE email = '$email'";
$result = $mysql->query($sql);
$user = $result->fetch_assoc();

if($user == null || count($user) == 0 || !password_verify($pass, $user['pass'] )){
    setcookie('error_aut', "Takového uživatele v systemu není", time() + 3600, "/");
    header('Location: index.php');
    exit();
}
setcookie('user', $user['name'], time() + 3600, "/");

$mysql->close();
setcookie('authorized', "ano", time() + 3600, "/");
header('Location: user.php');
?>
