<?php
/*Soubor kontroluje udaje zapsane ve formě změny uivatele(change.php)
a když jsou údaje spravné, udělavá update*/
$name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
$phone = filter_var(trim($_POST['phone']), FILTER_SANITIZE_STRING);
$pass = filter_var(trim($_POST['pass']), FILTER_SANITIZE_STRING);
$errors = 0;
setcookie('error_name', "Jmeno bylo zapsano nekorektně", time() - 3600, "/");
setcookie('error_name2', "Nekorektní delka jmena", time() - 3600, "/");
setcookie('error_phone', "Telefonní čislo je zapsano nekorektně)", time() - 3600, "/");
setcookie('error_pass', "Nekorektní delka hesla(od 2 do 10 symbolů)", time() - 3600, "/");
if(!preg_match("/^[a-zA-Z ]*$/",$name)){
    setcookie('error_name', "Jmeno bylo zapsano nekorektně", time() + 3600, "/");
    $errors++;
}
if(mb_strlen($name) > 30 || mb_strlen($name) < 2){
    setcookie('error_name2', "Nekorektní delka jmena", time() + 3600, "/");
    $errors++;
}
if((mb_strlen($phone) !=  12 && mb_strlen($phone) != 9) || !is_numeric($phone)){
    setcookie('error_phone', "Telefonní čislo je zapsano nekorektně)", time() + 3600, "/");
    $errors++;
}
if($pass != null){
    if(mb_strlen($pass) > 10 || mb_strlen($pass) < 3){
        setcookie('error_pass', "Nekorektní delka hesla(od 2 do 10 symbolů)", time() + 3600, "/");
        $errors++;
    }
}
if($errors > 0){
    header('Location: change.php');
    exit();
}
require "connect.php";
$userId = $_COOKIE['userId'];
$sql = 0;
    if($pass != null){
        $sql = "SELECT * FROM Employee WHERE userId = '$userId'";
        $result = $mysql->query($sql);
        $user = $result->fetch_assoc();
        $email = $user['email'];
        $message = "Dobrý den.".'<br>'."Změnily jste svoje přihlašovaci udaje".'<br>'."Vaš login: ".$email.'<br>'."Vaše heslo: ".$pass;
        $to = $email;
        $from = "admin@mail.ua";
        $subject = "Změna. Provider";
        $subject = "=?utf-8?B?".base64_encode($subject)."?=";
        $headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/plain; charset= utf-f\r\n";

        if(mail($to, $subject, $message, $headers) === TRUE){
            echo '<br>'."Otpravleno";
        }

        $pass = password_hash($pass, PASSWORD_DEFAULT);
        $sql = "UPDATE Employee SET name = '$name', phone = '$phone', pass = '$pass' WHERE Employee.userId = '$userId'; ";
    }else{
        $sql = "UPDATE Employee SET name = '$name', phone = '$phone' WHERE Employee.userId = '$userId'; ";
    }
setcookie('userId', $userId, time() - 3600, "/");
if($mysql->query($sql) === TRUE){
    echo "Record created";
}
$mysql->close();

header('Location: user.php');