<?php
require "userHead.php";
/*soubor slouží pro vyvedení tabulky z všemi pujčky co byli (Vracene a nevracene)*/
?>
    <div class="container mt-4">
    <?php
if (isset($_GET['offset'])) {
    $offset = (int)$_GET['offset'];
} else {
    $offset = 1;
}
require "connect.php";
$sql = "SELECT * FROM Loans";
$result = $mysql->query($sql);
$user = $result->fetch_all(MYSQLI_ASSOC);
$count = count($user);
if(count($user) > 0) { ?>
    <table class="table table-striped table-hover">
        <tr>
            <th>ID</th>
            <th>Pujčujicí</th>
            <th>Vec</th>
            <th>Datum pujčení</th>
            <th>Datum vracení</th>
            <th>Je vraceno?</th>
        </tr>

        <?

        for($a = ($offset - 1) * 10; $a < $offset * 10; $a++){
            if($a > $count - 1){
                continue;}
            $returned = $user[$a]['returned'];
            if($returned == 0){
                $returned = "Ne";
            }
            if($returned == 1){
                $returned = "Ano";
            }

            $dataP = $user[$a]['borrowDate'];
            $dataV = $user[$a]['renewDate'];
            $id = $user[$a]['loanId'];
            $userId = $user[$a]['userId'];
            $productId = $user[$a]['productId'];
            $sql = "SELECT * FROM Employee WHERE userId = '$userId'";
            $result = $mysql->query($sql);
            $use = $result->fetch_assoc();
            $name = $use['name'];
            $sql = "SELECT * FROM Goods WHERE productId = '$productId'";
            $result = $mysql->query($sql);
            $use = $result->fetch_assoc();
            $title = $use['title'];
            ?>

            <tr>
                <td>
                    <? echo $id; ?>
                </td>
                <td>
                    <? echo $name; ?>
                </td>
                <td>
                    <? echo $title; ?>
                </td>
                <td>
                    <? echo $dataP; ?>
                </td>
                <td>
                    <? echo $dataV; ?>
                </td>
                <td>
                    <? echo $returned; ?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <div>
        <ul class=" pagination justify-content-center">
            <?php
            for($i=1; $i<=ceil($count/10); $i++){
                echo '<li class="page-item"><a class="text-info page-link" href="allLoans.php?offset='.$i.'">'.$i.'</a></li>';
            }
            ?>
        </ul>
    </div>
    <br />
    <?php
}
$mysql->close();
require "footer.php";
?>