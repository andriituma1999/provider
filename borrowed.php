<!DOCTYPE html>
<html lang="cs">
<?php
/*Soubor slouží jako prvni stranka, kterou vidí user po přihlašení
Vyvádí tabulku s pujčenymi zboži a s možnosti vratit*/

?>

<head>
    <title>Provider - semestrální práce z předmětu 4IZ278</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>

<body>
<header class="container bg-info">
    <div class="row">
        <div class="col-7">
            <h1 class="text-white py-1 px-2">Provider</h1>
            <h5 class="text-white py-1 px-2">
                <?php

                $email;
                $name;
                $vedouci;
                $phone;
                $myId = 0;
                if(isset($_COOKIE['registered'])){
                    $email = $_COOKIE['r_email'];
                    $name = $_COOKIE['r_name'];
                    $phone = $_COOKIE['r_phone'];
                    $vedouci = $_COOKIE['r_vedouci'];
                    require "connect.php";
                    $sql = "SELECT * FROM Employee WHERE userId = '$vedouci'";
                    $result = $mysql->query($sql);
                    $user = $result->fetch_assoc();
                    $vedouci = $user['name'];
                    $sql = "SELECT * FROM Employee WHERE email = '$email'";
                    $result = $mysql->query($sql);
                    $user = $result->fetch_assoc();
                    $myId = $user['userId'];
                    echo "Jmeno: ".$name.'<br>'."Telefonní čislo: ".$phone.'<br>'."Email: ".$email.'<br>'."Vedoucí: ".$vedouci.'<br>';
                    // setcookie('registered', "ne", time() - 3600, "/");
                    $mysql->close();
                }
                if(isset($_COOKIE['authorized'])){
                    $email = $_COOKIE['a_email'];
                    require "connect.php";
                    $sql = "SELECT * FROM Employee WHERE email = '$email'";
                    $result = $mysql->query($sql);
                    $user = $result->fetch_assoc();
                    $name = $user['name'];
                    $phone = $user['phone'];
                    $vedouci = $user['idVed'];
                    $myId = $user['userId'];
                    $mysql->close();
                    require "connect.php";
                    $sql = "SELECT * FROM Employee WHERE userId = '$vedouci'";
                    $result = $mysql->query($sql);
                    $user = $result->fetch_assoc();
                    $vedouci = $user['name'];
                    echo "Jmeno: ".$name.'<br>'."Telefonní čislo: ".$phone.'<br>'."Email: ".$email.'<br>'."Vedoucí: ".$vedouci.'<br>';
                    // setcookie('authorized', "ne", time() - 3600, "/");
                    $mysql->close();
                }

                ?>
            </h5>
            <?php  if($myId != 1){?>
                <a href="user.php"  class="btn btn-success" name = "fd" >Free devices</a>
                <a href="borrowed.php"  class="btn btn-success" name = "fd" >My devices</a>
                <a href="change.php" class="btn btn-success" name = "os" >Změnit info</a>
                <a href="exit.php" class="btn btn-success" name = "os" >Odhlasit se</a>
            <?}else{?>
                <a href="user.php"  class="btn btn-success" name = "fd" >Free devices</a>
                <a href="borrowed.php"  class="btn btn-success" name = "fd" >My devices</a>
                <a href="change.php" class="btn btn-success" name = "os" >Změnit info</a>
                <a href="allLoans.php" class="btn btn-success" name = "os" >Všechny pujčky</a>
                <a href="changeGood.php" class="btn btn-success" name = "os" >Zboži</a><br><br>
                <a href="exit.php" class="btn btn-success" name = "os" >Odhlasit se</a>
            <?}?>
        </div>
    </div>
</header>

<div class ="container mt-4" >
    <?php
    if (isset($_GET['offset'])) {
        $offset = (int)$_GET['offset'];
    } else {
        $offset = 1;
    }
    require "connect.php";

    $sql = "SELECT * FROM Loans WHERE userId = '$myId' and returned = FALSE ";
    $result = $mysql->query($sql);
    $user = $result->fetch_all(MYSQLI_ASSOC);
    $count = count($user);
    if(count($user) > 0) { ?>
    <table class="table table-striped table-hover">
        <tr>
        <th>Nazev</th>
            <th>Producer</th>
            <th>Data pujčení</th>
            <th>Data vracení</th>
            <th>Vratit</th>
        </tr>

<?

for($a = ($offset - 1) * 10; $a < $offset * 10; $a++){
    if($a > $count - 1){
        continue;}
        $dataP = $user[$a]['borrowDate'];
        $dataV = $user[$a]['renewDate'];
    $productid = $user[$a]['productId'];
    $sql = "SELECT * FROM Goods WHERE productId = '$productid'";
    $result = $mysql->query($sql);
    $use = $result->fetch_assoc();
    $title = $use['title'];
    $producerid = $use['producerId'];
    $sql = "SELECT * FROM Producer WHERE producerId = '$producerid'";
    $result = $mysql->query($sql);
    $use = $result->fetch_assoc();
    $brand = $use['title'];
    ?>
    <tr>
        <td>
            <? echo $title; ?>
        </td>
        <td>
            <? echo $brand; ?>
        </td>
        <td>
            <? echo $dataP; ?>
        </td>
        <td>
            <? echo $dataV; ?>
        </td>
        <td>
            <a class="btn btn-danger" href="return.php?returned=<?=$a+1?>" role="button">-</a>
        </td>
    </tr>
<?php } ?>
    </table>
        <div>
            <ul class=" pagination justify-content-center">
                <?php
                for($i=1; $i<=ceil($count/10); $i++){
                    echo '<li class="page-item"><a class="text-info page-link" href="borrowed.php?offset='.$i.'">'.$i.'</a></li>';
                }
                ?>
            </ul>
        </div>
        <br />
<?
    }
    $mysql->close();
    require "footer.php";
    ?>
